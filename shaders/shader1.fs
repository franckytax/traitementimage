#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D slimeTexture;
uniform sampler2D lavaTexture;
uniform sampler2D waterTexture;

void main()
{
    FragColor = texture(lavaTexture, TexCoord) * vec4(ourColor, 1.0);
}