#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

//scalar between 0.0f and 1.0f sin
uniform float scalarColor;
//horizontal offset between -1.0 and 1.0f
//uniform float offsetX;
//render color pass to the fragment shader
out vec3 ourColor;

out vec2 TexCoord;

void main()
{
    //move the box on the x abcissa
    gl_Position = vec4(aPos, 1.0);
    //gl_Position = vec4(aPos, 1.0);
    
    ourColor = scalarColor * aColor;
    
    //ourColor = aColor;
    TexCoord = aTexCoord;
}