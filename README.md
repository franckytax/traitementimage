# TraitementImage

Utilisation des connaissances acquises en cours de traitement d'image

## Compilation sous Linux

### Prérequis :
* GLFW version 3.3
* OpenGL version 3.3 minimum (commande pour vérifier la version d'OpenGL : glxinfo | grep "OpenGL core profile version")
* cmake version 3.0.0 minimum (commande pour vérifier la version de cmake: cmake --version)
* Environnement X11

### 1. Préparation des fichiers de compilation
    A la racine du projet, créez un nouveau dossier build et s'y déplacer avec la commande ci-dessous:

    mkdir build && cd build

    Générer les fichiers nécessaires à la compilation du projet en spécifiant le répertoire relatif du CMakeLists.txt:

    cmake ..
    
### 2. Compilation du projet
    Lancer la compilation du projet avec la commande ci-dessous:

    make

## Exécution du projet
    Lancer la commande suivante pour exécuter le binaire TraitementImage produit lors de l'étape précédente:

    ./TraitementImage
    