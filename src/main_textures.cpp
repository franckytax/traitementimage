#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include "Shader.h"
#include "stb_image.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
void loadTextureFromFile(unsigned int texture, const char *filename);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

int main()
{
    //Init GLFW, require minimum version 3.3 of OpenGL and tell to use core profile
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    GLFWwindow *window = glfwCreateWindow(800, 600, "Traitement d'image", NULL, NULL);
    if (window == NULL) {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    //make the context of our window the main context on the current thread. 
    glfwMakeContextCurrent(window);

    //initialize GLAD
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    //size of the rendering window with a width of 800 and a height of 600
    glViewport(0, 0, 800, 600);

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    //display the current opengl version used
    int major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
    int minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);

    std::cout << "OpenGL version currently used : OpenGL Core profile version " 
    << major << "." << minor << std::endl;


    //load shaders
    Shader triangleShader("../shaders/shader1.vs", "../shaders/shader1.fs");

    //deleteShaders(fragmentShaders);
    

    //set up vertex data

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------

    float vertices[] {
            // positions    // colors               // texture coords
        1.0f,  1.0f, 0.0f,  1.0f, 0.0f, 0.0f,       4.0f, 4.0f,   // top right
        1.0f, -1.0f, 0.0f,  1.0f, 1.0f, 0.0f,       4.0f, 0.0f,   // bottom right
        -1.0f, -1.0f, 0.0f, 1.0f, 0.5f, 0.0f,       0.0f, 0.0f,   // bottom left
        -1.0f,  1.0f, 0.0f, 1.0f, 0.286f, 0.286f,   0.0f, 4.0f    // top left 
    };

    //A buffer of nbTriangle x 3 indexes
    unsigned int indices[] = {
        0, 1, 3, //first triangle
        1, 2, 3  //second triangle
    };

    //vertex buffer object : store a large number of vertices in GPU's memory
    //vertex array object
    //element buffer object
    unsigned int VBO, VAO, EBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    //position attribute in location 0 : 3 compenents: x, y and z
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //color attribute in location 1: 3 components : r, g and b
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);

    //texture coords attribute in location 2: 2 components: x and y
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //unbind VAO
    glBindVertexArray(0);

     //texture object generation
    unsigned int textures[3];
    glGenTextures(3, textures);
    
    //load the textures
    loadTextureFromFile(textures[0], "../textures/slime.jpg");
    loadTextureFromFile(textures[1], "../textures/lava.jpg");
    loadTextureFromFile(textures[2], "../textures/water.jpg");


    //set uniform sampler2D values in the fragment shader
    triangleShader.use();
    triangleShader.setInt("slimeTexture", 0);
    triangleShader.setInt("lavaTexture", 1);
    triangleShader.setInt("waterTexture", 2);


    //render loop
    while(!glfwWindowShouldClose(window))
    {
        //input
        processInput(window);

        // render
        glClearColor(0.25f, 0.6f, 0.6f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        //bind texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textures[0]);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textures[1]);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textures[2]);

        glBindVertexArray(VAO);

        //draw the top left triangle in red, change in function of the time
        
        //get the time in seconds
        float timeValue = glfwGetTime();
        
        // scalarColor for lava animation
        float scalarColor = abs(sin(timeValue) / 4.0f) + 0.4f;
        //offsetX takes values between -1.0 and 1.0f
        //float offsetX = cos(timeValue);
        
        triangleShader.use();
        //set the value of the scalar variable in the shader program
        
        triangleShader.setFloat("scalarColor", scalarColor);
        //triangleShader.setFloat("offsetX", offsetX);

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // check and call events and swap the buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    glDeleteTextures(3, textures);
    
    triangleShader.clean();

    glfwTerminate();
    return 0;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    } else if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
        //draw the rectangle in wireframe mode
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else if (glfwGetKey(window, GLFW_KEY_L) == GLFW_RELEASE) {
        //pass the rectangle in fill mode
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    
}

void loadTextureFromFile(unsigned int texture, const char *textureFilename)
{
    //bind the texture
    glBindTexture(GL_TEXTURE_2D, texture);

    //confifure the texture wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    //configure the texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    stbi_set_flip_vertically_on_load(true);

    int width, height, nrChannels;
    unsigned char *data = stbi_load(textureFilename, &width, &height, &nrChannels, 0);

    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "Failed to load texture from file " << textureFilename << std::endl;
    }
    stbi_image_free(data);
}