#ifndef HSV_H
#define HSV_H

/**
 * @brief The Hue, Saturation and Value model
 * 
 */
class HSV
{
    public:
        /**
         * @brief Construct a new HSV object
         * By default, Hue, Saturation and Brightness are equals to 0
         * 
         */
        HSV();

        /**
         * @brief Construct a new HSV object
         * 
         * @param hue The color hue
         * @param saturation The color saturation
         * @param brightness The color brightness
         */
        HSV(float hue, float saturation, float brightness);

        /**
         * @brief Get the color hue
         * 
         * @return float color hue
         */
        float getHue() const;

        /**
         * @brief Get the color saturation
         * 
         * @return float color saturation
         */
        float getSaturation() const;

        /**
         * @brief Get the color brightness
         * 
         * @return float color brightness
         */
        float getBrightness() const;

        /**
         * @brief Set the color hue
         * 
         * @param hue color hue
         */
        void setHue(float hue);

        /**
         * @brief Set the color saturation
         * 
         * @param saturation color saturation
         */
        void setSaturation(float saturation);

        /**
         * @brief Set the color brightness
         * 
         * @param brightness color brightness
         */
        void setBrightness(float brightness);

        /**
         * @brief Print the HSV values
         * 
         */
        void print();

    private:
        float m_hue;
        float m_saturation;
        float m_brightness;
};

#endif