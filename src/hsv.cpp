#include "hsv.h"
#include "math.h"
#include <iostream>
#include <cmath>

HSV::HSV() : m_hue(0.0f), m_saturation(0.0f), m_brightness(0.0f)
{}

HSV::HSV(float hue, float saturation, float brightness)
{
    float intpart;
    if (hue > 1.0f or hue < 0.0f)
    {
        m_hue = abs(modf(hue, &intpart));
    }
    else
    {
        m_hue = hue;
    }

    m_saturation = saturation;
    m_brightness = brightness;
}

float HSV::getHue() const
{
    return m_hue;
}

float HSV::getSaturation() const
{
    return m_saturation;
}

float HSV::getBrightness() const
{
    return m_brightness;
}

void HSV::setHue(float hue)
{
    float intpart;
    if (hue > 1.0f or hue < 0.0f)
    {
        m_hue = abs(modf(hue, &intpart));
    }
    else
    {
        m_hue = hue;
    }
}

void HSV::setSaturation(float saturation)
{
    m_saturation = saturation;
}

void HSV::setBrightness(float brightness)
{
    m_brightness = brightness;
}

void HSV::print()
{
    std::cout << "hue :" << m_hue << ", saturation :" 
    << m_saturation << ", brightness :" << m_brightness << std::endl;
}